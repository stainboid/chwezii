import colors from 'vuetify/es5/util/colors'

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
   
     meta: [
      { charset: 'utf-8' },
      { name: 'theme-color', content: '#000000' },
      { name: 'msapplication-navbutton-color' , content: '#000000' },
      { name: 'apple-mobile-web-app-capable' , content: 'yes' },
      {name: 'monetization' , content:'$ilp.uphold.com/KeZqRj2YkFGf'},
      { name: 'apple-mobile-web-app-status-bar-style'  , content: '#000000' },
      {name:'google-signin-client_id' , content: '806400696724-7tqb6svov0roiglcbb6kg6mghu45tcig.apps.googleusercontent.com' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Discover the magic of African internet servers at kachaq, a rootsman powered entertainment sensation. Raise your spirits with funny jokes, trending memes, entertaining African gifs, inspiring stories, viral videos, and so much more.' },
      { hid: 'description', name: 'keywords', content: 'funny, image, gif, gifs, memes, jokes, image upload, upload image, lol, humor, sell,buy, comment, share, kachaq, kachaq.com, wallpaper,African wilderness ,photography' },
      { name: 'google-site-verification', content: 'mgG2YZ_pevMwM8SqZrcbe9h3y51XomoXOhT0DFt1u_E' },
      
    ],
    link: [
      //{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {rel: 'canonical', href:'https://kachaq.com'},
       
      
      {rel:'stylesheet', href:'https://fonts.googleapis.com/css?family=ABeeZee|Crimson+Text|Special+Elite&display=swap'}
    ],
    script: [
      { src: 'https://apis.google.com/js/platform.js' }
    ],
  },
  /*
  ** Customize the progress-bar color
  */ 
loading: { color: "#3b8070", throttle: 0 },

  generate: {
routes: function () {
return [
'/image/1',
'/image/2',
'/image/3',
 '/image/4',
'/image/5',
'/image/6',
 '/image/7',
'/image/8',
'/image/9',
'/image/10',
'/image/11',
'/image/12',

'/recipe/1',
'/recipe/2',
'/recipe/3',
'/recipe/4',
'/recipe/5',
];
}
},
  /*
  ** Global CSS
  */
  css: [
    '~/assets/main.css',
      '~/assets/styles.css',
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: "~/plugins/aos", ssr: false },
    { src: "~/plugins/vue-masonry-css", ssr: false },
    { src: '~plugins/ga.js', mode: 'client' },
    { src: '~plugins/SocialSharing.js', mode: 'client' }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/vuetify',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/onesignal',
    '@nuxtjs/pwa',
    '@nuxtjs/auth',
    '@nuxtjs/sitemap',
     ['@nuxtjs/google-adsense', {
      id: 'ca-pub-5388314784840597'
    }],
    '@nuxtjs/firebase'
    
  ],
    firebase: {
// options

config: {
        apiKey: 'AIzaSyAq0Ev1EOBB3lP3w_WiyKBGvpElfY-M1aE',
        authDomain: 'kachaq.firebaseapp.com',
        databaseURL: 'https://kachaq.firebaseio.com',
        projectId: 'kachaq',
        storageBucket: 'kachaq.appspot.com',
        messagingSenderId: '806400696724',
        appId: '1:806400696724:web:8b191efcee438de5dc6f81',
        measurementId: 'G-Q0WN6V0C9F'
      },
services: {
        auth: true ,
        messaging: true,
        performance: true,
        analytics: true// Just as example. Can be any other service.
      }

},
  
  sitemap: {
    hostname: 'https://kachaq.com/',
    lastmod: '2019-11-13',
    sitemaps: [
      {
        path: '/sitemap.xml',
        routes: [
            'image', 
        'search' , 
        'image/1',
        'image/2',
        'recipe',
        'recipe/1',
        'recipe/2',
        'recipe/3'
        ],
        gzip: true
      },
    ]
  },
  
  
  manifest: {
      short_name: 'kachaq',
    name: 'Kachaq',
    lang: 'en',
    start_url: '/image/',
  background_color: '#000000',
  display: 'standalone',
  scope: '/image/',
  theme_color: '#000000'

  },
   icon: {
    /* icon options */
    iconSrc:'/assets/icon.png',
  },
   workbox: {
    /* workbox options */
    runtimeCaching:[
        {
            urlPattern:'https://fonts.googleapis.com/.*',
            handler:'cacheFirst',
            method:'GET',
            strategyOptions:{ cacheableResponse:{statuses:[0,200]}}
            
        },
        ]
  },

  


  //auth: {
  // Options
//},

  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    //customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
      
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
